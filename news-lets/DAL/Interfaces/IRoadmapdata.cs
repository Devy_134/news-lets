﻿using System.Collections.Generic;
using System.Threading.Tasks;
using news_lets.Entities;

namespace news_lets.DAL
{
    public interface IRoadmapdata
    {
        Task<List<Roadmap>> GetRoadMap();
    }
}