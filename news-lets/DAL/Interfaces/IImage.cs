﻿using Microsoft.AspNetCore.Http;
using news_lets.Enums;
using System.Threading.Tasks;

namespace news_lets.DAL
{
    public interface IImage
    {
        Task<string> GeneratePathForImage(ImageTypeEnum imageType,string userId);
        Task<string> SaveImage(string filePath, IFormFile formFile);
    }
}