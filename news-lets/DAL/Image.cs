﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using news_lets.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DAL
{
    public class Image : IImage
    {
        private IHostingEnvironment _environment;
        public Image(IHostingEnvironment environment)
        {
            _environment = environment;
        }
        public async Task<string> SaveImage(string filePath,IFormFile formFile)
        {
            string path= filePath.Substring(0, filePath.LastIndexOf('/'));
            System.IO.Directory.CreateDirectory(path);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await formFile.CopyToAsync(fileStream);
            }
            return (filePath.Substring(filePath.LastIndexOf("wwwroot") + 7));
        }

        public async Task<string> GeneratePathForImage(ImageTypeEnum imageType,string userId)
        {
            string path=null;
            switch (imageType)
            {
                case ImageTypeEnum.Logo:
                    path = nameof(ImageTypeEnum.Logo);
                    break;
                case ImageTypeEnum.PageBackground:
                    path = nameof(ImageTypeEnum.PageBackground);
                    break;
                case ImageTypeEnum.ArticleImage:
                    path = "Article/" + nameof(ImageTypeEnum.ArticleImage);
                    break;
                case ImageTypeEnum.TwitterImage:
                    path = "Twitter/" + nameof(ImageTypeEnum.TwitterImage);
                    break;
            }
            var webRoot = _environment.WebRootPath;
            return webRoot+$"/img/{path}/{userId}.png";
        }
    }
}
