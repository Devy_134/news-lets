﻿using Microsoft.EntityFrameworkCore;
using news_lets.Entities;
using news_lets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DAL
{
    public class Roadmapdata : IRoadmapdata
    {
        private readonly news_letsContext _news_LetsContext;
        public Roadmapdata(news_letsContext news_LetsContext)
        {
            _news_LetsContext = news_LetsContext;
        }
        public async Task<List<Roadmap>> GetRoadMap()
        {
            return await _news_LetsContext.Roadmaps.OrderByDescending(x => x.Expected).ToListAsync();
        }
    }
}
