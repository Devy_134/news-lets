﻿using System;
using System.Collections.Generic;
using System.Text;

namespace news_lets.Models
{
    public class ViewsResults
    {
        public string Id { get; set; }
        public string Views { get; set; }
        public string ValueType { get; set; }
    }
}
