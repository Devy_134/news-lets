﻿$(document).ready(function () {
    $.ajax({
        url: '/Account/GetUserDescription',
        type: 'post',
        success: function (data, textStatus, jQxhr) {
            if (data !== null) {         
            $("#UserName").text(data.userName);
            $("#UserDescription").text(data.userText);
            $("#UserLogo").attr('src', data.accountLogo);
            var backgroud = data.backgroungSkin;
                $("#UserSkin").css('background-image', ' url("' + backgroud + '")');
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
    $('#holder').on('click', "h1.editable", function () {
        var $lbl = $(this), o = $lbl.text(),
            $txt = $('<input type="text" class="editable-label-text" value="' + o + '" />');
        $lbl
            .replaceWith($txt);
        $txt.focus();

        $txt.blur(function () {
            $txt.replaceWith($lbl);
        })
            .keydown(function (evt) {
                if (evt.keyCode === 13) {
                    var no = $(this).val();
                    $lbl.text(no);
                    $txt.replaceWith($lbl);
                }
            });
    });

    $('#UserBackgroundInput').change(function (e) {
        var fileName = e.target.files[0].name;
        $('#UserBackgroundInputText').html(fileName);
    });

    $('#UserLogoInput').change(function (e) {
        var fileName = e.target.files[0].name;
        $('#UserLogoInputText').html(fileName);
    });

    function PreviewChanges() {
        var UserSkin = $('#customFile').prop('files');
        var UserLogo = $('#customFile1').prop('files');
        $('#UserLogo').attr('src', URL.createObjectURL(UserLogo[0]));
        $('#UserSkin').css('background-image', ' url("' + URL.createObjectURL(UserSkin[0]) + '")');
    }

    $('#customSwitches').change(function (e) {
        if ($(this).is(":checked")) {
            $('#customFile').prop('disabled', false);
        }
        else {
            $('#customFile').prop('disabled', true);
        }
    });

    $('#customSwitches1').change(function (e) {
        if ($(this).is(":checked")) {
            $('#customFile1').prop('disabled', false);
        }
        else {
            $('#customFile1').prop('disabled', true);
        }
    });

    //setting needed event handlers
    $('#save').click(SaveDescription);
    $('#DescriptionPage').click(LoadDescriptionData);
    $('#AritclesPage').click(LoadArticlesData);
    $('#EmailPage').click(LoadEmailData);
    $('#UserLogo').click(ChangeLogoPopup);
    $('#PreviewImagesBtn').click(PreviewChanges);

    function SaveDescription() {
        var UserName = $('#UserName').text();
        var UserDescription = $('#UserDescription').val();
        var UserSkin = $('#customFile').prop('files')[0];
        var UserLogo = $('#customFile1').prop('files')[0];
        var userDescriptionViewModel = new FormData();
        userDescriptionViewModel.append('UserName', UserName);
        userDescriptionViewModel.append('UserDescription', UserDescription);
        userDescriptionViewModel.append('UserSkin', UserSkin);
        userDescriptionViewModel.append('UserLogo', UserLogo);
        $.ajax({
            type: "POST",
            url: "/Account/SaveUserDescriptionChanges",
            data: userDescriptionViewModel,
            contentType: false,
            processData: false
        });
    }

    function LoadDescriptionData() {
        $('#AritclesPage').removeClass('active');
        $('#EmailPage').removeClass('active');
        $('#DescriptionPage').addClass('active');
        window.location.href = '/Account';
    }

    function LoadArticlesData() {
        $('#AritclesPage').addClass('active');
        $('#EmailPage').removeClass('active');
        $('#DescriptionPage').removeClass('active');
        $('#content').empty();
        var card_container = `<div class="card-columns" id="cardContainer">  
        </div> `;
        $('#content').prepend(card_container);
        $.ajax({
            type: "POST",
            url: "/Article/GetArticles",
            dataType: "json",
            success: function (response) {
                response.forEach(function (responseItem) {
                    var fields = responseItem.split(/\*/);
                    var name = fields[0];
                    var id = fields[1];
                    var card = `<div class="card bg-info">
                    <div class="card-header">${name}</div>
                    <div class="card-body text-center text-white" id="${id}">  
                    <h5 class="text-center">
                    <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" onclick="deleteAricle('${id}')" class="btn btn-secondary">Delete</button>
                    <button type="button" onclick="editArticle('${id}')" class="btn btn-secondary">Edit</button>
                    <button type="button" onclick="viewArticle('${id}')" class="btn btn-secondary">View</button>
                    </div>
                    </h5>  
                    </div>
                    <div class="card-footer text-muted">
                       2 days ago
                     </div>
                    </div> `;
                    $('#cardContainer').append(card);
                });
                $('#content').css("padding-bottom", "");
                var AddArticleCard = `<div class="card bg-info">
                <div class="card-body text-center text-white" onclick="redirectToCreatePage()">  
                    <p class="card-text"><i class="fas fa-plus"></i></p>  
                </div>  
            </div> `;
                $('#cardContainer').append(AddArticleCard);
            },
            error: function (response) {
                console.log("not");
            }
        });

        //TODO:Load articles as json
        //put loading gif here
    }

    function LoadEmailData() {
        $('#AritclesPage').removeClass('active');
        $('#EmailPage').addClass('active');
        $('#DescriptionPage').removeClass('active');
        $.ajax({
            url: '/Account/GetEmailAmounts',
            type: 'post',
            success: function (data, textStatus, jQxhr) {
                $('#content').empty();
                var html = `<h1 class="display-4">Your subscriber list has <strong>${data}</strong> subscribers.</h1>
                <p class="lead">
                <a class="btn btn-primary btn-lg" onclick="ExportEmails()" role="button">Export</a>
                </p>`;
                $('#content').append(html);
                console.log(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    function ChangeLogoPopup() {
        $('#myModal').modal('show');
    }
});

function ExportEmails() {
    $.ajax({
        url: '/Account/GetAllSubscribers',
        type: 'post',
        success: function (data, textStatus, jQxhr) {
            var csvContent = '';
            data.forEach(function (infoArray, index) {
                dataString = infoArray;
                csvContent += index < data.length ? dataString + '\n' : dataString;
            });

            // The download function takes a CSV string, the filename and mimeType as parameters
            // Scroll/look down at the bottom of this snippet to see how download is called
            var download = function (content, fileName, mimeType) {
                var a = document.createElement('a');
                mimeType = mimeType || 'application/octet-stream';

                if (navigator.msSaveBlob) { // IE10
                    navigator.msSaveBlob(new Blob([content], {
                        type: mimeType
                    }), fileName);
                } else if (URL && 'download' in a) { //html5 A[download]
                    a.href = URL.createObjectURL(new Blob([content], {
                        type: mimeType
                    }));
                    a.setAttribute('download', fileName);
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);
                } else {
                    location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
                }
            };

            download(csvContent, 'SubscrierList.csv', 'text/csv;encoding:utf-8');
        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function redirectToCreatePage() {
    window.location.href = "/Article/ArticleCreation";
}

function deleteAricle(articleId) {
    $.ajax({
        type: "POST",
        data: { articleId: articleId },
        url: "/Article/Delete",
        success: function () {
            var divid = "#" + articleId;
            $(divid).parent().remove();
        },
        error: function () { }
    });
}

function editArticle(id) {

}

function viewArticle(id) {
    window.location.href = "/Article/Show/"+id;
}