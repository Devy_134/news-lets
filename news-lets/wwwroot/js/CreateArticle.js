﻿var canExpand = true;
$(document).ready(function () {
    $("#example-vertical").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });
    $(".actions ul li:nth-child(2) a").hide();
    $('#Title').change(ActivateBtnNext);
    $('#exampleFormControlTextarea1').change(ActivateBtnNext);
    //$('#customFile1').change(ActivateBtnNext);
    $('#Alt').change(ActivateBtnNext);
    $('#ArticleDescription').change(ActivateBtnNext);
    $('#AddVid').click(AddVideo);

    $.ajax({
        type: "POST",
        url: "/Article/GetTopics",
        data: null,
        contentType: false,
        processData: false,
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                var html = `<option value="${response[i].topicId}">${response[i].topicName}</option>`;
                $('#drop').append(html);
            }
            $('.selectpicker').selectpicker('refresh');
            $('.selectpicker').selectpicker('render');
        },
        error: function (response) {
            var html = `
                <div class="alert alert-danger" role="alert">
                  Something went wrong, please try again later :(
                </div>`;
            $('#articleForm').append(html);
        }
    });

    $('#TwitterImag').change(function (e) {
        var id = '#TwitterImage';
        ChangeNameAndActivateBtn(e, id);
    });

    $('#customFile1').change(function (e) {
        var id = '#ArticleImage';
        ChangeNameAndActivateBtn(e,id);
    });

    function ChangeNameAndActivateBtn(e, id) {
        var fileName = e.target.files[0].name;
        $(id).html(fileName);
        ActivateBtnNext();
    }

    function AddVideo() {
        if (canExpand) {
             var html = `<div class="form-row" id="AdditionalVideo">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Video Title">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Youtube link">
                    </div>
                    <div class="col">
                        <button type="button" class="btn btn-danger mb-2" id="RemoveVid">-</button>
                    </div>
                </div>`;
            $('#Videos').append(html);
            $('#RemoveVid').click(RemoveVideo);
            canExpand = false;
        }      
    }

    function RemoveVideo() {
        if (!canExpand) {
            $('#AdditionalVideo').remove();
            canExpand = true;
        }
    }

    function ActivateBtnNext() {
        if ($('#example-vertical').steps("getCurrentIndex") === 0) {
            if ($('#Title').val() !== "" && $('#exampleFormControlTextarea1').val() !== "" && $('#customFile1').val() !== "") {
                $(".actions ul li:nth-child(2) a").show();
                $(".actions ul li:nth-child(2) a").click(DisableNext);
            }
            else {
                $(".actions ul li:nth-child(2) a").hide();
            }
        }
        else if ($('#example-vertical').steps("getCurrentIndex") === 1) {
            if ($('#Alt').val() !== "" && $('#ArticleDescription').val() !== "") {
                $(".actions ul li:nth-child(2) a").show();
                $(".actions ul li:nth-child(2) a").click(DisableFinal);
            }
            else {
                $(".actions ul li:nth-child(2) a").hide();
            }
        }
        else if ($('#example-vertical').steps("getCurrentIndex") === 2) {
            if ($('#TwitterTitle').val() !== "" && $('#TwitterDescription').val() !== "" && $('#TwitterImag').val() !== "") {
                $(".actions ul li:nth-child(3) a").click(SendCall);
                $(".actions ul li:nth-child(3) a").show();
            }
            else {
                $(".actions ul li:nth-child(3) a").hide();
            }
        }
    }
    function SendCall() {
        //building topics string
        var TopicsToSend;
        if ($('#drop').val().length !== 0) {
            TopicsToSend = $('#drop').val().toString();
        }
        TopicsToSend += `,${$('#Topic').val()}`;

        //Getting Youtube Videos and making those into a call
        var Inputs = $('#Videos').children();
        var YoutubeVideo=[];
        for (var i = 0; i < Inputs.length; i++) {
            var s = Inputs[i].children;
            var Title = s[0].firstElementChild.value;
            var StringToReconstruct = s[1].firstElementChild.value.split("/");
            var Link = StringToReconstruct[StringToReconstruct.length - 1].replace("watch?v=","");
            YoutubeVideo.push({ VideoTitle: Title, VideoCode: Link });
        }

        var userDescriptionViewModel = new FormData();
        userDescriptionViewModel.append('Title', $('#Title').val());
        userDescriptionViewModel.append('Text', $('#exampleFormControlTextarea1').val());
        userDescriptionViewModel.append('Image', $('#customFile1').prop('files')[0]);
        userDescriptionViewModel.append('Alt', $('#Alt').val());
        userDescriptionViewModel.append('AltText', $('#ArticleDescription').val());
        userDescriptionViewModel.append('TwitterTitle', $('#TwitterTitle').val());
        userDescriptionViewModel.append('TwitterDescription', $('#TwitterDescription').val());
        userDescriptionViewModel.append('Topics', TopicsToSend);
        userDescriptionViewModel.append('TwitterImage', $('#TwitterImag').prop('files')[0]);
        //userDescriptionViewModel.append('YoutubeVideo', YoutubeVideo);
        $(YoutubeVideo).each(function (index, el) {
            userDescriptionViewModel.append(`YoutubeVideo[${index}].VideoTitle`, el.VideoTitle);
            userDescriptionViewModel.append(`YoutubeVideo[${index}].VideoCode`, el.VideoCode);
        });

        $.ajax({
            type: "POST",
            url: "/Article/Create",
            data: userDescriptionViewModel,
            contentType: false,
            processData: false,
            success: function (response) {
                $('#articleForm').empty();
                var html = `
                <div class="alert alert-success" role="alert">
                  Your <a href="${response}" class="alert-link">article</a> was published!
                </div>`;
                $('#articleForm').append(html);
            },
            error: function (response) {
                var html = `
                <div class="alert alert-danger" role="alert">
                  Something went wrong, please try again later :(
                </div>`;
                $('#articleForm').append(html);
            }
        });
    }
    function DisableNext() {
        $(".actions ul li:nth-child(2) a").hide();
    }
    function DisableFinal() {
        $(".actions ul li:nth-child(3) a").hide();
    }
});
