﻿$(document).ready(function () {
    ChangeToArticles(3);
    LoadTopics();
    LoadTopStory();
});

function LoadTopics() {
    $.ajax({
        type: "POST",
        url: "/Article/GetMostPopularTopics",
        contentType: false,
        success: function (result) {
            $('#topics').empty();
            for (var i = 0; i < result.length; i++) {
                var html = `<span class="badge badge-secondary">${result[i].topicName}</span>`;
                $('#topics').append(html);
            }

        }
    });
}

function LoadTopStory() {
    $.ajax({
        type: "POST",
        url: "/Article/GetLatestNewsLetsArticle",
        contentType: false,
        success: function (result) {
            $("#TopImage").prop("src", result.imagePath);
            $("#TopImage").prop("alt", result.alt);
            $("#TopTitle").text(result.title);
            $("#TopText").html($(result.text).text().substring(0, 170));
            $("#TopDate").text(new Date(result.createdDate).toUTCString());
            console.log(result);
        }
    });
}

function ChangeToAuthors() {
    document.getElementById("newslink").classList.remove("active");
    document.getElementById("authorslink").classList.add("active");
    $.ajax({
        type: "POST",
        url: "/Author/TopThreeData",
        contentType: false,
        success: function (result) {
            $('#place').empty();
            for (var i = 0; i < result.length; i++) {
                if (result[i].userName === null) {
                    result[i].userName = "Anonymous";
                }
                var html = `<div class="card" style="width: 18rem;">
                    <img src="${result[i].logoUrl}" width="292" height="292" onError="this.onerror=null;this.src='/img/NoLogo.png';" id="ArticleImage1" class="card-img-top" alt="${result[i].userName}">
                    <div class="card-body text-center">
                    <h5 class="card-title" id="ArticleTitle1">${result[i].userName}</h5>
                    <a href="/Author/Show?authorId=${result[i].applicationUserId}" id="ArticleLink1" class="btn btn-primary">Visit</a>
                    </div>
                    </div>`;
                $('#place').append(html);
            }

        }
    });

}
function ChangeToArticles(amount) {
    document.getElementById("authorslink").classList.remove("active");
    document.getElementById("newslink").classList.add("active");
    $.ajax({
        type: "POST",
        url: "/Article/GetNewest?amount=" + amount,
        contentType: false,
        success: function (result) {
            $('#place').empty();
            for (var i = 0; i < result.length; i++) {
                var html = `<div class="card" style="width: 18rem;">
                    <img src="${result[i].imagePath}" width="355" height="355" id="ArticleImage1" class="card-img-top" alt="${result[i].alt}">
                    <div class="card-body">
                    <h5 class="card-title" id="ArticleTitle1">${result[i].title}</h5>
                    <p class="card-text text-muted" id="ArticleText1">${result[i].twitterDescription}</p>
                    <a href="/Article/Show/${result[i].articleId}" id="ArticleLink1" class="btn btn-primary">Visit</a>
                    </div>
                    </div>`;
                $('#place').append(html);
            }

        }
    });
}