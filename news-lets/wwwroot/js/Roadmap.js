﻿$(document).ready(function () {
    $.ajax({
        url: '/Home/RoadMapData',
        type: 'GET',
        success: function (data, textStatus, jQxhr) {
            for (var i = 0; i < data.length; i++) {
                //randomize to determine wheter to display on left or bot
                var side = Math.floor((Math.random() * 2) + 1);
                var sideclass = "";
                var doneclass = "";
                var color = Math.floor((Math.random() * 5) + 1);
                var colorclass = "";
                if (side === 1) {
                    sideclass = "";
                }
                else {
                    sideclass = "timeline-inverted";
                }
                if (data[i].done === true) {
                    doneclass = "far fa-check-square";
                }
                else {
                    doneclass = "far fa-square";
                }
                if (color === 1) {
                    colorclass = "danger";
                }
                else if (color===2) {
                    colorclass = "info";
                }
                else if (color === 3) {
                    colorclass = "warning";
                }
                else if (color === 4) {
                    colorclass = "";
                }
                else if (color === 5) {
                    colorclass = "success";
                }
                var html = `<li class="${sideclass}">
                <div class="timeline-badge ${colorclass}"><i class="${doneclass}"></i></div>
                    <div class="timeline-panel">
                <div class="timeline-heading">
                    <h4 class="timeline-title">Version: ${data[i].version}</h4>
                    <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i>Expected: ${new Date(parseInt(data[i].expected.substr(6)))}</small></p>
                </div>
                <div class="timeline-body">
                    <p>${data[i].changes}</p>
                </div>
                </div>
            </li>`;
                $('.timeline').prepend(html);
            }
            var html2 = `<li class="timeline-inverted">
                <div class="timeline-badge success" id="show"><i class="fas fa-plus" data-toggle="modal" data-target="#exampleModal"></i></div>
                    <div class="timeline-panel">
                <div class="timeline-heading">
                    <h4 class="timeline-title">Click on + to request feature</h4>
                    <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i></small></p>
                </div>
                <div class="timeline-body">
                    <p>Your requests will be reviewed and potentially included in next release</p>
                </div>
                </div>
            </li>`;
            $('.timeline').prepend(html2);
            $('#show').click(openModal);
            $('#sendSuggestion').click(Send);
        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
    function openModal() {
        $('#exampleModalLong').modal();
    }
    function Send() {
        var email = $('#exampleFormControlInput1').val();
        var text = $('#message-text').val();
        var datatoSend = { "Email": email, "Text": text };
        $.ajax({
            contentType: 'application/json',
            data: JSON.stringify(datatoSend),
            dataType: 'json',
            processData: false,
            type: 'POST',
            url: '/Home/RoadMapSuggestion'
        });
    }
});