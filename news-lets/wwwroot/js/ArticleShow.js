﻿$(document).ready(function () {
    $('#exampleModalCenter').modal('show');
    $('#SubscribeBtnSubmit').click(SubscribeToAuthor);
    $('#CommentSubmitBtn').click(SumbitComment);
});

function openModal(Youtube) {
    var youtubeEmbedLink = "https://www.youtube.com/embed/" + Youtube +"?autoplay=1";
    $('#ytplayer').attr("src", youtubeEmbedLink);
    $('#exampleModalLong').modal();
}

$(function () {
    var data = [
    ];
    //Get All article Names for the search
    $.ajax({
        type: "POST",
        url: "/Article/GetAllArticleNames",
        contentType: "application/json",
        dataType: "json",
        success: function (result) {
            for (var i = 0; i < result.length; i++) {
                data.push({ label: `${result[i].articleName}`, category: "Article", value: `/Article/Show/${result[i].articleId }` });
            }
        }
    });
    //Get All author Names for the search
    $.ajax({
        type: "POST",
        url: "/Author/GetAllAuthorNames",
        contentType: "application/json",
        dataType: "json",
        success: function (result) {
            for (var i = 0; i < result.length; i++) {
                data.push({ label: `${result[i].authorName}`, category: "Author", value: `/Author/Show?authorId=${result[i].authorId}` });
            }
        }
    });
    var data1 = [
        { label: "anders", category: "", value: "http://www.google.com" },
        { label: "andreas", category: "" },
        { label: "antal", category: "" },
        { label: "annhhx10", category: "Products" },
        { label: "annk K12", category: "Products" },
        { label: "annttop C13", category: "Products" },
        { label: "anders andersson", category: "People" },
        { label: "andreas andersson", category: "People" },
        { label: "andreas johnson", category: "People" }
    ];
    $("#search").autocomplete({
        source: data,
        select: function (event, ui) {
            window.location = (ui.item.value);
            return false;
        },
        _renderMenu: function (ul, items) {
            var that = this,
                currentCategory = "";
            $.each(items, function (index, item) {
                var li;
                if (item.category !== currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                li = that._renderItemData(ul, item);
                if (item.category) {
                    li.attr("aria-label", item.category + " : " + item.label);
                }
            });
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li>")
            .data("ui-autocomplete-item", item)
            .append(item.category+"<br><a>" + item.label + "</a>")
            .appendTo(ul);
    };

});

function SubscribeToAuthor() {
    var email = $('#Email').val();
    var articleId = $('#articleId').text();
    var subscriptionViewModel = new FormData();
    subscriptionViewModel.append('articleId', articleId);
    subscriptionViewModel.append('email', email);
    $.ajax({
        type: "POST",
        url: "/Article/Subscribe",
        data: subscriptionViewModel,
        contentType: false,
        processData: false,
        success: function () {
            $('#exampleModalCenter').modal('hide');
        }
    });
}

function SumbitComment() {
    var pseudonim = $('#Pseudonim').val();
    var comment = $('#comment').val();
    var articleId = $('#articleId').text();
    var commentViewModel = new FormData();
    commentViewModel.append('pseudonim', pseudonim);
    commentViewModel.append('comment', comment);
    commentViewModel.append('articleId', articleId);
    $.ajax({
        type: "POST",
        url: "/Article/Comment",
        data: commentViewModel,
        contentType: false,
        processData: false,
        success: function () {
            location.reload();
        }
    });
}