﻿$(document).ready(function () {
    $('#ArticlesTable').DataTable();
});

function redirectToArticlePage(authorId) {
    window.location.href = "/Article/Show?id=" + authorId;
}