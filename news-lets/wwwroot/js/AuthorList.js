﻿$(document).ready(function () {
    $('#AuthorsTable').DataTable();
});

function redirectToAuthorPage(authorId) {
    window.location.href = "/Author/Show?authorId=" + authorId;
}