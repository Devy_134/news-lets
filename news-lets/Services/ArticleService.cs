﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using news_lets.BLL;
using news_lets.DAL;
using news_lets.DataContracts;
using news_lets.Entities;
using news_lets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IImage _image;
        private readonly news_letsContext _news_LetsContext;
        private readonly ITelegramWebHook _telegramWebHook;
        public ArticleService(IImage image, news_letsContext news_LetsContext,ITelegramWebHook telegramWebHook)
        {
            _image = image;
            _news_LetsContext = news_LetsContext;
            _telegramWebHook = telegramWebHook;
        }

        public async Task<ArticleShowDC> GetLatestNewsLetsArticle()
        {
            Article article = (await _news_LetsContext.ApplicationUserArticles.Include(x=>x.Article)
                .Where(x => x.ApplicationUser.UserDescription.UserName == "News-Lets").OrderByDescending(x=>x.Article.CreatedDate).FirstOrDefaultAsync()).Article;
            return article.Adapt<ArticleShowDC>();
        }

        public async Task<List<ArticleShowDC>> GetArticlesList()
        {
            List<Article> articles = await _news_LetsContext.articles.Include(x => x.ArticleTopics).ToListAsync();
            return articles.Adapt<List<ArticleShowDC>>();
        }

        public async Task<List<(string, string)>> GetAllArticleNames()
        {
            List<(string, string)> articleNamesIds = new List<(string, string)>();
            foreach(Article article in (await _news_LetsContext.articles.ToListAsync()))
            {
                ValueTuple<string, string> articleNameId = (article.Title, article.ArticleId);
                articleNamesIds.Add(articleNameId);
            }
            return articleNamesIds;
        }

        public async Task<List<string>> GetArticleNames(string userId)
        {
            List<string> articleIds = await _news_LetsContext.ApplicationUserArticles.Where(x => x.ApplicationUserId == userId).Select(x=>x.ArticleId).ToListAsync();
            List<string> articleNames = new List<string>();
            foreach(string articleId in articleIds)
            {
                articleNames.Add(await _news_LetsContext.articles.Where(x => x.ArticleId == articleId).Select(x => x.Title).SingleAsync()+"*"+await _news_LetsContext.articles.Where(x => x.ArticleId == articleId).Select(x => x.ArticleId).SingleAsync());
            }
            return articleNames;
        }

        public async Task<List<TopicsDC>> GetTopics()
        {
            return (await _news_LetsContext.Topics.ToListAsync()).Adapt<List<TopicsDC>>();
        }

        public async Task<List<TopTopicsDC>> GetMostPopularTopics()
        {
            List<TopTopicsDC> topTopicsDCs = new List<TopTopicsDC>();
            List<TopTopicsDC> topTopicsToReturn = new List<TopTopicsDC>();
            foreach (Topic topic in _news_LetsContext.Topics.Include(x=>x.ArticleTopics).ToList())
            {
                TopTopicsDC topTopicsDC = new TopTopicsDC();
                topTopicsDC.TopicName = topic.TopicName;
                topTopicsDC.Occurences = topic.ArticleTopics.Count;
                topTopicsDCs.Add(topTopicsDC);
            }
            topTopicsToReturn=topTopicsDCs.OrderByDescending(x => x.Occurences).ToList();
            return topTopicsToReturn;
        }

        public async Task<List<ArticleShowDC>> GetNewestArticles(int amount)
        {
            List<Article> articles = await _news_LetsContext.articles.OrderByDescending(x=>x.CreatedDate).ToListAsync();
            List<Article> articlesToReturn = new List<Article>();
            if(articles.Count>=amount)
            { 
                for (int i=0;i<amount;i++)
                {
                    Article article = articles[i];
                    articlesToReturn.Add(article);
                }
            }
            else
            {
                articlesToReturn = articles;
            }
            return articlesToReturn.Adapt<List<ArticleShowDC>>();
        }

        public async Task<ArticleShowDC> GetArticleData(string articleId)
        {
            Article article = await _news_LetsContext.articles.Include(x=>x.ArticleTopics).Include(x=>x.YoutubeVideo).FirstOrDefaultAsync(x => x.ArticleId == articleId);
            ArticleShowDC articleShowDC = article.Adapt<ArticleShowDC>();
            List<string> topics = new List<string>();
            foreach(ArticleTopic articleTopic in article.ArticleTopics)
            {
                Topic topic = _news_LetsContext.Topics.FirstOrDefault(x => x.TopicId == articleTopic.TopicId);
                if(topic!=null && topic.TopicName!=null)
                { 
                    topics.Add(topic.TopicName);
                }
            }
            articleShowDC.Topics = topics;
            return articleShowDC;
        }

        public async Task Comment(CommentDC commentDC)
        {
            Article article = await _news_LetsContext.articles.Include(x=>x.Comments).FirstOrDefaultAsync(x => x.ArticleId == commentDC.articleId);
            Entities.Comment comment = commentDC.Adapt<Entities.Comment>();
            if(article.Comments==null)
            {
                ICollection<Comment> comments = new List<Comment>();
                comments.Add(comment);
                article.Comments = comments;
            }
            else
            {
                article.Comments.Add(comment);
            }
            await _news_LetsContext.SaveChangesAsync();
        }

        public async Task<List<Comment>> GetComments(string articleId)
        {
            ICollection<Comment> comments = (await _news_LetsContext.articles.Include(x => x.Comments).FirstOrDefaultAsync(x => x.ArticleId == articleId)).Comments;
            return comments.ToList<Comment>();
        }

        public async Task<string> Save(string userId, ArticleCreateDC articleCreateDC)
        {
            Article article = articleCreateDC.Adapt<Article>();
            ApplicationUser applicationUser= _news_LetsContext.ApplicationUsersArticles.FirstOrDefault(x => x.Id == userId);
            string description = article.Text;
            List<string> paragraphs = new List<string>();
            foreach (var line in description.Split('\n'))
            {
                paragraphs.Add("<p>" + line + "</p>");
            }
            article.Text = string.Concat(paragraphs.ToArray());
            article.CreatedDate = DateTime.UtcNow;
            ApplicationUserArticle application = new ApplicationUserArticle() { ApplicationUser = applicationUser, Article = article };
            //Topics distilation
            string[] topics = articleCreateDC.Topics.Split(",");
            _news_LetsContext.ApplicationUserArticles.Add(application);
            foreach(string topic in topics)
            {
                if(!string.IsNullOrEmpty(topic))
                { 
                    if(topic.All(Char.IsDigit))
                    {
                        int topicId = int.Parse(topic);
                        ArticleTopic articleTopic = new ArticleTopic();
                        articleTopic.Topic = await _news_LetsContext.Topics.FirstOrDefaultAsync(x => x.TopicId == topicId);
                        articleTopic.Article = article;
                        _news_LetsContext.ArticleTopics.Add(articleTopic);
                    }
                    else
                    {
                        //Here we create new topics
                        Topic topic1 = new Topic();
                        topic1.TopicName = topic;
                        ArticleTopic articleTopic = new ArticleTopic();
                        articleTopic.Topic = topic1;
                        articleTopic.Article = article;
                        _news_LetsContext.ArticleTopics.Add(articleTopic);
                    }
                }
            }
            
            if (articleCreateDC.TwitterImage != null)
            {
                string path = await _image.GeneratePathForImage(Enums.ImageTypeEnum.TwitterImage, article.ArticleId);
                article.TwitterImage=await _image.SaveImage(path, articleCreateDC.TwitterImage);
            }
            if (articleCreateDC.Image != null)
            {
                string path = await _image.GeneratePathForImage(Enums.ImageTypeEnum.ArticleImage, article.ArticleId);
                article.ImagePath=await _image.SaveImage(path, articleCreateDC.Image);
            }
            await _news_LetsContext.SaveChangesAsync();            
            return application.ArticleId;
        }

        public async Task Subscribe(SubscriptionDC subscriptionDC)
        {
            ApplicationUserArticle applicationUserArticle = await _news_LetsContext.ApplicationUserArticles.Include(x=>x.ApplicationUser).FirstOrDefaultAsync(x => x.ArticleId == subscriptionDC.articleId);
            if(applicationUserArticle.ApplicationUser.Subscribers==null)
            {
                List<string> subscribers = new List<string>();
                subscribers.Add(subscriptionDC.email);
                applicationUserArticle.ApplicationUser.Subscribers = subscribers;
            }
            else
            {
                applicationUserArticle.ApplicationUser.Subscribers.Add(subscriptionDC.email);
            }
            await _news_LetsContext.SaveChangesAsync();
        }

        public async Task Delete(string userId, string articleId)
        {
            Article article = await _news_LetsContext.articles.Include(x=>x.Comments).Include(x=>x.YoutubeVideo).FirstOrDefaultAsync(x=>x.ArticleId==articleId);
            ApplicationUserArticle applicationUserArticle = await _news_LetsContext.ApplicationUserArticles.FirstOrDefaultAsync(x => x.ArticleId == article.ArticleId);
            List<ArticleTopic> articleTopics = await _news_LetsContext.ArticleTopics.Where(x => x.ArticleId == articleId).ToListAsync();
            foreach(ArticleTopic topic in articleTopics)
            {
                _news_LetsContext.ArticleTopics.Remove(topic);
            }
            ICollection<Comment> comments = article.Comments;
            foreach(Comment comment in comments)
            {
                _news_LetsContext.Comments.Remove(comment);
            }
            ICollection<YoutubeVideo> youtubeVideos = article.YoutubeVideo;
            foreach(YoutubeVideo video in youtubeVideos)
            {
                _news_LetsContext.YoutubeVideos.Remove(video);
            }
            _news_LetsContext.ApplicationUserArticles.Remove(applicationUserArticle);
            _news_LetsContext.articles.Remove(article);
            await _news_LetsContext.SaveChangesAsync();
        }

        public async Task<List<ArticleShowDC>> GetSuggestArticles(List<string> topicNames)
        {
            List<Article> articles = await _news_LetsContext.ArticleTopics
                .Include(x => x.Article)
                .Include(x => x.Topic)
                .Where(x => topicNames.Contains(x.Topic.TopicName)).Select(x => x.Article).ToListAsync();
            return articles.Adapt<List<ArticleShowDC>>();
        }
    }
}
