﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace news_lets.Services
{
    public class EmailSender : IEmailSender
    {
        public EmailSender(IOptions<AuthMessageSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public AuthMessageSenderOptions Options { get; } //set only via Secret Manager

        public Task SendEmailAsync(string email, string subject, string message)
        {
            return Execute(subject, message, email,Options.SendGridKey);
        }

        public Task Execute(string subject, string message, string email,string apiKey)
        {
            var client = new SendGridClient("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");
            var msg = new SendGridMessage()
            {
                From = new EmailAddress("real.news.lets@gmail.com", "News-Lets"),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };
            msg.AddTo(new EmailAddress(email));

            // Disable click tracking.
            // See https://sendgrid.com/docs/User_Guide/Settings/tracking.html
            msg.SetClickTracking(false, false);

            return client.SendEmailAsync(msg);
        }
    }
}
