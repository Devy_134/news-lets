﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using news_lets.DataContracts;
using news_lets.Entities;
using news_lets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly news_letsContext _news_LetsContext;
        public AuthorService(news_letsContext news_LetsContext)
        {
            _news_LetsContext = news_LetsContext;
        }

        public async Task<List<AuthorNameIdDC>> GetAllAuthorNames()
        {
            List<AuthorNameIdDC> authorNameIdDCs = new List<AuthorNameIdDC>();
            foreach(ApplicationUser author in await _news_LetsContext.Users.Include(x=>x.UserDescription).ToListAsync())
            {
                AuthorNameIdDC authorNameIdDC = new AuthorNameIdDC() {
                    AuthorName = author.UserDescription!=null?author.UserDescription.UserName:string.Empty,
                    AuthorId = author.Id
                };
                authorNameIdDCs.Add(authorNameIdDC);
            }
            return authorNameIdDCs;
        }

        public async Task<List<AuthorDC>> GetAllAuthorDescriptions()
        {
            List<ApplicationUser> applicationUsers = await _news_LetsContext.Users
                .Include(x=>x.ApplicationUserArticles)
                .ThenInclude(x=>x.Article)
                .Include(x=>x.UserDescription)
                .ToListAsync();
            List<AuthorDC> authorDCs = new List<AuthorDC>();
            foreach(ApplicationUser applicationUser in applicationUsers)
            {
                AuthorDC authorDC = new AuthorDC();
                authorDC.ApplicationUserId = applicationUser.Id;
                if(applicationUser.UserDescription!=null)
                {
                    authorDC.UserName = applicationUser.UserDescription.UserName;
                    authorDC.LogoUrl = applicationUser.UserDescription.AccountLogo;
                }
                if(applicationUser.Subscribers!=null)
                {
                    authorDC.Subscribers = applicationUser.Subscribers.Count;
                }
                if(applicationUser.ApplicationUserArticles!=null)
                {
                    authorDC.ArticleCount = applicationUser.ApplicationUserArticles.Count;
                }
                authorDC.Views = 10;
                authorDCs.Add(authorDC);
            }
            return authorDCs;
        }

        public async Task<AuthorShowDC> GetAuthorShow(string id)
        {
            AuthorShowDC authorShowDC = new AuthorShowDC();
            ApplicationUser applicationUser = await _news_LetsContext.Users
                .Include(x => x.ApplicationUserArticles)
                .ThenInclude(x => x.Article)
                .Include(x => x.UserDescription)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (applicationUser.UserDescription != null)
            {
                authorShowDC.UserDescription = applicationUser.UserDescription.Adapt<UserDescriptionJsonDC>();
            }
            if(applicationUser.ApplicationUserArticles!=null)
            {
                authorShowDC.articleShowViewModels = applicationUser.ApplicationUserArticles.Select(x => x.Article).ToList().Adapt<List<ArticleShowDC>>();
            }

            return authorShowDC;
        }
    }
}
