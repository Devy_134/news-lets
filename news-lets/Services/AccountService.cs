﻿using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using news_lets.DAL;
using news_lets.DataContracts;
using news_lets.Entities;
using news_lets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.Services
{
    public class AccountService : IAccountService
    {
        private readonly IImage _image;
        private readonly news_letsContext _news_LetsContext;
        private readonly ITelegramWebHook _telegramWebHook;
        public AccountService(IImage image, news_letsContext news_LetsContext,ITelegramWebHook telegramWebHook)
        {
            _image = image;
            _news_LetsContext = news_LetsContext;
            _telegramWebHook = telegramWebHook;
        }

        public async Task SaveDescription(string userId,UserDescriptionDC userDescriptionDC)
        {           
            UserDescription userDescription = new UserDescription();
            userDescription.UserText = userDescriptionDC.UserDescription;
            userDescription.UserName = userDescriptionDC.UserName;
            if(userDescriptionDC.UserSkin!=null)
            { 
                string path=await _image.GeneratePathForImage(Enums.ImageTypeEnum.PageBackground,userId);
                userDescription.BackgroungSkin = await _image.SaveImage(path,userDescriptionDC.UserSkin);
            }
            if(userDescriptionDC.UserLogo!=null)
            {
                string path = await _image.GeneratePathForImage(Enums.ImageTypeEnum.Logo, userId);
                userDescription.AccountLogo = await _image.SaveImage(path, userDescriptionDC.UserLogo);
            }
            //check if description already exists for this user
            ApplicationUser applicationUser = await _news_LetsContext.ApplicationUsersArticles.Include(x=>x.UserDescription).FirstOrDefaultAsync(x => x.Id == userId);
            userDescription.ApplicationUserId = userId;
            if(applicationUser.UserDescription!=null)
            {
                UserDescription userDescription1= _news_LetsContext.userDescriptions.FirstOrDefault(x => x.ApplicationUser.Id == userId);
                userDescription1.UserText = userDescription.UserText;
                userDescription1.UserName = userDescription.UserName;
                if(userDescription.BackgroungSkin!=null)
                {
                    userDescription1.BackgroungSkin = userDescription.BackgroungSkin;
                }
                if(userDescription.AccountLogo!=null)
                {
                    userDescription1.AccountLogo = userDescription.AccountLogo;
                }               
                _news_LetsContext.SaveChanges();
            }
            else
            {
                _news_LetsContext.Add(userDescription);
                _news_LetsContext.SaveChanges();
            }
        }

        public async Task<string[]> GetAccountNameId(string articleId)
        {
            ApplicationUserArticle applicationUserArticle = await _news_LetsContext.ApplicationUserArticles.Include(x=>x.ApplicationUser).ThenInclude(y=>y.UserDescription).FirstOrDefaultAsync(x => x.ArticleId == articleId);
            string[] nameId;
            if (applicationUserArticle.ApplicationUser.UserDescription!=null)
            { 
                string[] TempName = {applicationUserArticle.ApplicationUser.Id,applicationUserArticle.ApplicationUser.UserDescription.UserName };
                nameId = TempName;
            }
            else
            {
                string[] TempName = { applicationUserArticle.ApplicationUser.Id, "Name" };
                nameId = TempName;
            }
            return nameId;
        }

        public async Task<UserDescriptionJsonDC> GetUserDescription(string userId)
        {
            UserDescription userDescription = await _news_LetsContext.userDescriptions.FirstOrDefaultAsync(x => x.ApplicationUserId == userId);
            return userDescription.Adapt<UserDescriptionJsonDC>();
        }

        public async Task<int> GetSubscriberAmount(string authorId)
        {
            int subCount = 0;
            ApplicationUser applicationUser = await _news_LetsContext.Users.FirstOrDefaultAsync(x => x.Id == authorId);
            if(applicationUser.Subscribers!=null)
            {
                subCount = applicationUser.Subscribers.Count();
            }
            return subCount;
        }

        public async Task<List<string>> GetAllSubscribers(string authorId)
        {
            return (await _news_LetsContext.Users.FirstOrDefaultAsync(x => x.Id == authorId)).Subscribers;
        }
    }
}
