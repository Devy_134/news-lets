﻿using news_lets.DataContracts;
using news_lets.Entities;
using news_lets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.Services
{
    public class ExternalService : IExternalService
    {
        private readonly news_letsContext _news_LetsContext;

        public ExternalService(news_letsContext news_LetsContext)
        {
            _news_LetsContext = news_LetsContext;
        }

        public async Task UpdateViews(List<ViewsResultsDC> viewsResultsDC)
        {
            if(viewsResultsDC!=null && viewsResultsDC.Count!=0)
            {
                foreach(ViewsResultsDC viewsResult in viewsResultsDC)
                {
                    if(viewsResult.ValueType == "Article")
                    {
                        Article article = _news_LetsContext.articles.FirstOrDefault(x => x.ArticleId == viewsResult.Id);
                        if(article != null)
                        {
                            int s;
                            Int32.TryParse(viewsResult.Views, out s);
                            article.Views = s;
                            _news_LetsContext.Update(article);
                            await _news_LetsContext.SaveChangesAsync();
                        }
                    }
                    else if(viewsResult.ValueType == "Author")
                    {
                        UserDescription userDescription = _news_LetsContext.userDescriptions.FirstOrDefault(x => x.ApplicationUserId == viewsResult.Id);
                        if (userDescription != null)
                        {
                            int s;
                            Int32.TryParse(viewsResult.Views, out s);
                            userDescription.Views = s;
                            _news_LetsContext.Update(userDescription);
                            await _news_LetsContext.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
        }
    }
}
