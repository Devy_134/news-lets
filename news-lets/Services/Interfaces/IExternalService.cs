﻿using news_lets.DataContracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace news_lets.Services
{
    public interface IExternalService
    {
        Task UpdateViews(List<ViewsResultsDC> viewsResultsDC);
    }
}