﻿using System.Collections.Generic;
using System.Threading.Tasks;
using news_lets.DataContracts;

namespace news_lets.Services
{
    public interface IRoadMapService
    {
        Task<List<RoadmapDC>> GetRoadMap();
    }
}