﻿using System.Collections.Generic;
using System.Threading.Tasks;
using news_lets.DataContracts;
using news_lets.Entities;

namespace news_lets.Services
{
    public interface IArticleService
    {
        Task<ArticleShowDC> GetLatestNewsLetsArticle();
        Task<List<ArticleShowDC>> GetArticlesList();
        Task<List<(string, string)>> GetAllArticleNames();
        Task<List<TopTopicsDC>> GetMostPopularTopics();
        Task<List<TopicsDC>> GetTopics();
        Task<List<ArticleShowDC>> GetNewestArticles(int amount);
        Task<List<Comment>> GetComments(string articleId);
        Task Comment(CommentDC commentDC);
        Task Subscribe(SubscriptionDC subscriptionDC);
        Task Delete(string userId, string articleId);
        Task<List<string>> GetArticleNames(string userId);
        Task<string> Save(string userId, ArticleCreateDC articleCreateDC);
        Task<ArticleShowDC> GetArticleData(string articleId);
        Task<List<ArticleShowDC>> GetSuggestArticles(List<string> topicNames);
    }
}