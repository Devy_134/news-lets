﻿using System.Threading.Tasks;
using news_lets.DataContracts;

namespace news_lets.Services
{
    public interface ITelegramWebHook
    {
        Task SendSuggestion(RoadMapSuggestionDC roadMapSuggestionDC);
        Task SendError(string message);
    }
}