﻿using System.Collections.Generic;
using System.Threading.Tasks;
using news_lets.DataContracts;

namespace news_lets.Services
{
    public interface IAuthorService
    {
        Task<List<AuthorNameIdDC>> GetAllAuthorNames();
        Task<List<AuthorDC>> GetAllAuthorDescriptions();
        Task<AuthorShowDC> GetAuthorShow(string id);
    }
}