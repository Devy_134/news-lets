﻿using Mapster;
using news_lets.DAL;
using news_lets.DataContracts;
using news_lets.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.Services
{
    public class RoadMapService : IRoadMapService
    {
        private readonly IRoadmapdata _roadmapdata;

        public RoadMapService(IRoadmapdata roadmapdata)
        {
            _roadmapdata = roadmapdata;
        }

        public async Task<List<RoadmapDC>> GetRoadMap()
        {
            List<Roadmap> roadmaps = await _roadmapdata.GetRoadMap();
            return roadmaps.Adapt<List<RoadmapDC>>();
        }
    }
}
