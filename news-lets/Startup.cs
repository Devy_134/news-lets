using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using news_lets.BLL;
using news_lets.DAL;
using news_lets.DataContracts;
using news_lets.Entities;
using news_lets.Models;
using news_lets.Services;
using news_lets.ViewModels;

namespace news_lets
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddDbContext<news_letsContext>(options => options.UseNpgsql("Server=127.0.0.1;Port=5432;Database=news-lets;User Id=postgres;Password=admin;"));

            #region BLL Interface registration
            services.AddTransient<IAccountLogic, AccountLogic>();
            services.AddTransient<IRoadMapLogic, RoadMapLogic>();
            services.AddTransient<IArticleLogic, ArticleLogic>();
            services.AddTransient<IAuthorLogic, AuthorLogic>();
            services.AddTransient<IExternalLogic, ExternalLogic>();
            services.AddTransient<ILogger, Logger>();
            #endregion

            #region Services Interface registration
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IRoadMapService, RoadMapService>();
            services.AddTransient<ITelegramWebHook, TelegramWebHook>();
            services.AddTransient<IArticleService, ArticleService>();
            services.AddTransient<IAuthorService, AuthorService>();
            services.AddTransient<IExternalService, ExternalService>();
            #endregion

            #region DAL Interface registration
            services.AddTransient<IImage, Image>();
            services.AddTransient<IRoadmapdata, Roadmapdata>();
            #endregion
            TypeAdapterConfig.GlobalSettings.ForType<UserDescriptionViewModel, UserDescriptionDC>()
                .Ignore(x=>x.UserLogo)
                .Ignore(x=>x.UserSkin);
            TypeAdapterConfig.GlobalSettings.ForType<ArticleCreateViewModel, ArticleCreateDC>()
                .Ignore(x => x.Image)
                .Ignore(x => x.TwitterImage);
            TypeAdapterConfig.GlobalSettings.ForType<ArticleCreateDC, Article>();
            TypeAdapterConfig.GlobalSettings.Compile();
            //services.AddIdentity<ApplicationUser, IdentityRole>()
            //.AddEntityFrameworkStores<news_letsContext>()
            //.AddDefaultTokenProviders();  
            services.Configure<AuthMessageSenderOptions>(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });
        }
    }
}
