﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using news_lets.BLL;
using news_lets.ViewModels;

namespace news_lets.Controllers
{
    public class AuthorController : Controller
    {
        private readonly ILogger _logger;
        private readonly IAuthorLogic _authorLogic;
        public AuthorController(IAuthorLogic authorLogic,ILogger logger)
        {
            _authorLogic = authorLogic;
            _logger = logger;
        }

        public async Task<IActionResult> List()
        {
            AuthorsShowViewModel authorsShowViewModel = new AuthorsShowViewModel();
            authorsShowViewModel.authorViewModels=await _authorLogic.GetAllAuthorDescriptions();
            return View(authorsShowViewModel);
        }

        public async Task<IActionResult> GetAllAuthorNames()
        {
            List<AuthorNameId> authorNameIds = await _authorLogic.GetAllAuthorNames();
            return Json(authorNameIds);
        }

        public async Task<IActionResult> TopThreeData()
        {
            AuthorsShowViewModel authorsShowViewModel = new AuthorsShowViewModel();
            IEnumerable<AuthorViewModel> authors = (await _authorLogic.GetAllAuthorDescriptions()).OrderByDescending(x=>x.Subscribers).Take(3);
            return Json(authors);
        }

        public async Task<IActionResult> Show(string authorId)
        {
            try { 
                AuthorShowViewModel authorShowViewModel = await _authorLogic.GetAuthorData(authorId);
                foreach (ArticleShowViewModel article in authorShowViewModel.articleShowViewModels)
                {
                    article.Text = article.Text.Replace("<p>", "");
                    article.Text = article.Text.Replace("</p>", "");
                }
                return View(authorShowViewModel);
            }
            catch (Exception ex)
            {
                await _logger.SendError(ex);
                return View("Error");
            }
        }
    }
}