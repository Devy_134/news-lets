﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using news_lets.BLL;
using news_lets.DataContracts;
using news_lets.Models;
using news_lets.ViewModels;
using Serilog;

namespace news_lets.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRoadMapLogic _roadMapLogic;

        public HomeController(ILogger<HomeController> logger, IRoadMapLogic roadMapLogic)
        {
            _logger = logger;
            _roadMapLogic = roadMapLogic;
        }

        public IActionResult Index()
        {
            //Log.Information("Log: Log.Information");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult RoadMap()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public async Task<IActionResult> RoadMapData()
        {

            List<RoadmapDC> roadmapDCs = await _roadMapLogic.GetRoadMap();
            List<RoadViewModel> roadViewModels = roadmapDCs.Adapt<List<RoadViewModel>>();
            return Json(roadViewModels);

        }
        [HttpPost]
        public async Task RoadMapSuggestion([FromBody]RoadMapSuggestionViewModel roadMapSuggestionViewModel)
        {
            await _roadMapLogic.RoadMapSuggestion(roadMapSuggestionViewModel.Adapt<RoadMapSuggestionDC>());
        }
    }
}
