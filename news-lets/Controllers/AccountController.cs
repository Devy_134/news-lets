﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using news_lets.ViewModels;
using System.Security.Claims;
using news_lets.BLL;
using news_lets.DataContracts;
using Mapster;
using Microsoft.AspNetCore.Authorization;

namespace news_lets.Controllers
{   
    public class AccountController : Controller
    {
        private readonly IAccountLogic _accountLogic;
        private readonly ILogger _logger;

        public AccountController(IAccountLogic accountLogic,ILogger logger)
        {
            _accountLogic = accountLogic;
            _logger = logger;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public async Task SaveUserDescriptionChanges(UserDescriptionViewModel userDescriptionViewModel)
        {
            try { 
                var id = this.User.FindFirst(ClaimTypes.NameIdentifier);
                UserDescriptionDC userDescriptionDC = userDescriptionViewModel.Adapt<UserDescriptionDC>();
                userDescriptionDC.UserLogo = userDescriptionViewModel.UserLogo;
                userDescriptionDC.UserSkin = userDescriptionViewModel.UserSkin;
                await _accountLogic.SaveDescription(id.Value, userDescriptionDC);
            }
            catch(Exception ex)
            {
                await _logger.SendError(ex);
            }
        }

        public async Task<JsonResult> GetUserDescription()
        {
            try { 
            var id = this.User.FindFirst(ClaimTypes.NameIdentifier);
            UserDescriptionJsonDC userDescriptionJsonDC = await _accountLogic.GetUserDescription(id.Value);
            return Json(userDescriptionJsonDC);
            }
            catch (Exception ex)
            {
                await _logger.SendError(ex);
                UserDescriptionJsonDC userDescriptionJsonDC = new UserDescriptionJsonDC();
                return Json(userDescriptionJsonDC);
            }
        }

        [Authorize]
        public async Task<JsonResult> GetEmailAmounts()
        {
            int subscriberCount = 0;
            try
            {          
                var id = this.User.FindFirst(ClaimTypes.NameIdentifier);
                subscriberCount = await _accountLogic.GetSubscriberCount(id.Value);
            }
            catch (Exception ex)
            {
                await _logger.SendError(ex);
            }
            return Json(subscriberCount);
        }

        [Authorize]
        public async Task<JsonResult> GetAllSubscribers()
        {
            try { 
                var id = this.User.FindFirst(ClaimTypes.NameIdentifier);
                return Json(await _accountLogic.GetAllSubscribers(id.Value));
            }
            catch (Exception ex)
            {
                await _logger.SendError(ex);
                return Json(new EmptyResult());
            }
        }
    }
}