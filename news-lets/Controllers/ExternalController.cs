﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using news_lets.BLL;
using news_lets.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace news_lets.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExternalController : ControllerBase
    {
        // POST api/<ValuesController>

        private readonly IExternalLogic _externalLogic;
        private readonly ILogger _logger;
        public ExternalController(IExternalLogic externalLogic, ILogger logger)
        {
            _externalLogic = externalLogic;
            _logger = logger;
        }
        
        [HttpPost]
        public async Task PostAsync([FromBody] JsonElement value)
        {
            try { 
                List<ViewsResults> viewsResults = JsonSerializer.Deserialize<List<ViewsResults>>(value.GetRawText());
                if(viewsResults.Last().Id == "Djv5Qd2qjxxs7yjH" &&
                        viewsResults.Last().ValueType == "Code" &&
                        viewsResults.Last().Views == "576")
                { 
                    await _externalLogic.UpdateViews(viewsResults);
                }
            }
            catch(Exception ex)
            {
                await _logger.SendError(ex);
            }
        }

        [HttpGet]
        [Route("GenerateSiteMap")]
        public async Task GenerateSiteMap()
        {
            try
            {
                List<string> staticLinks = new List<string>();
                staticLinks.Add(this.Url.Action(nameof(HomeController.Index), "Home",null, this.Request.Scheme));
                staticLinks.Add(this.Url.Action(nameof(HomeController.Privacy), "Home",null, this.Request.Scheme));
                staticLinks.Add(this.Url.Action(nameof(HomeController.RoadMap), "Home",null, this.Request.Scheme));
                staticLinks.Add(this.Url.Action(nameof(HomeController.About), "Home",null, this.Request.Scheme));
                staticLinks.Add(this.Url.Action(nameof(ArticleController.List), "Article",null, this.Request.Scheme));
                staticLinks.Add(this.Url.Action(nameof(AuthorController.List), "Author",null, this.Request.Scheme));
                string siteUrl = this.Request.Scheme + "://" + this.Request.Host +"/";
                await _externalLogic.GenerateSiteMapAsync(staticLinks,siteUrl);
            }
            catch(Exception ex)
            {
                await _logger.SendError(ex);
            }
        }
    }
}
