﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using news_lets.BLL;
using news_lets.DataContracts;
using news_lets.ViewModels;
using Newtonsoft.Json;

namespace news_lets.Controllers
{
    public class ArticleController : Controller
    {
        private readonly IArticleLogic _articleLogic;
        private readonly IAccountLogic _accountLogic;
        private readonly ILogger _logger;
        public ArticleController(IArticleLogic articleLogic,IAccountLogic accountLogic,ILogger logger)
        {
            _articleLogic = articleLogic;
            _accountLogic = accountLogic;
            _logger = logger;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        //Don't remove this is important
        [Authorize]
        public IActionResult ArticleCreation()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            List<ArticleShowDC> articles=await _articleLogic.GetArticlesList();
            return View(articles.Adapt<List<ArticleShowViewModel>>());
        }

        public async Task<IActionResult> GetAllArticleNames()
        {
            List<(string, string)> articleNamesIds = await _articleLogic.GetAllArticleNames();
            List<ArticleNameId> articleNameIds = new List<ArticleNameId>();
            foreach(ValueTuple<string, string> articleNameId in articleNamesIds)
            {
                ArticleNameId articleNameId1 = new ArticleNameId();
                articleNameId1.ArticleId = articleNameId.Item2;
                articleNameId1.ArticleName = articleNameId.Item1;
                articleNameIds.Add(articleNameId1);
            }
            return Json(articleNameIds);
        }

        public async Task<IActionResult> GetLatestNewsLetsArticle()
        {
            ArticleShowViewModel articleShowViewModels = (await _articleLogic.GetLatestNewsLetsArticle()).Adapt<ArticleShowViewModel>();
            return Json(articleShowViewModels);
        }

        public async Task<IActionResult> GetNewest(int amount)
        {
            List<ArticleShowViewModel> articleShowViewModels=(await _articleLogic.GetNewestArticles(amount)).Adapt<List<ArticleShowViewModel>>();
            foreach (ArticleShowViewModel article in articleShowViewModels)
            {
                article.Text = article.Text.Replace("<p>", "");
                article.Text = article.Text.Replace("</p>", "");
            }
            return Json(articleShowViewModels);
        }

        public async Task<IActionResult> GetMostPopularTopics()
        {
            List<TopTopics> topTopics = new List<TopTopics>();
            try { 
                topTopics = (await _articleLogic.GetMostPopularTopics()).Adapt<List<TopTopics>>();
            }
            catch (Exception ex)
            {
                await _logger.SendError(ex);
            }
            return Json(topTopics);
        }

        public async Task<IActionResult> Show(string id)
        {
            try
            {
                ArticleShowViewModel articleShowViewModel = (await _articleLogic.GetArticleData(id)).Adapt<ArticleShowViewModel>();
                string[] NameId = await _accountLogic.GetAccountNameId(id);
                articleShowViewModel.AuthorId = NameId[0];
                articleShowViewModel.AuthorName = NameId[1];
                articleShowViewModel.ArticleNameIds = new List<ArticleNameId>();
                foreach(ArticleShowDC articleShowDC in await _articleLogic.GetSuggestArticles(articleShowViewModel.Topics))
                {
                    articleShowViewModel.ArticleNameIds.Add(new ArticleNameId()
                    {
                        ArticleName = articleShowDC.Title,
                        ArticleId = articleShowDC.ArticleId
                    });
                }                
                List<CommentDC> commentDCs =await _articleLogic.GetComments(id);
                if(commentDCs!=null)
                {
                    articleShowViewModel.Comments = commentDCs.Adapt<List<CommentViewModel>>();
                }
                return View(articleShowViewModel);
            }
            catch(Exception ex)
            {
                await _logger.SendError(ex);
                return View("Error");
            }
        }

        public async Task<IActionResult> GetTopics()
        {
            try { 
                return Json(await _articleLogic.GetTopics());
            }
            catch (Exception ex)
            {
                await _logger.SendError(ex);
                return Json(new object());
            }
        }

        public async Task Comment(CommentViewModel commentViewModel)
        {
            try { 
                await _articleLogic.Comment(commentViewModel.Adapt<CommentDC>());
            }
            catch (Exception ex)
            {
                await _logger.SendError(ex);
            }
        }

        public async Task Subscribe(SubscriptionViewModel subscriptionViewModel)
        {
            try { 
                await _articleLogic.Subscribe(subscriptionViewModel.Adapt<SubscriptionDC>());
            }
            catch (Exception ex)
            {
                await _logger.SendError(ex);
            }
        }

        public async Task<JsonResult> GetArticles()
        {
            try
            {
                var id = this.User.FindFirst(ClaimTypes.NameIdentifier);
                List<string> articleNames = await _articleLogic.GetAllArticles(id.Value);
                string json = JsonConvert.SerializeObject(articleNames);
                return Json(articleNames);
            }
            catch (Exception ex)
            {
                await _logger.SendError(ex);
                List<string> articleNames = new List<string>();
                return Json(articleNames);
            }
        }
        [Authorize]
        public async Task<IActionResult> Create(ArticleCreateViewModel articleCreateViewModel)
        {
            try { 
                ArticleCreateDC articleCreateDC = articleCreateViewModel.Adapt<ArticleCreateDC>();
                articleCreateDC.Image = articleCreateViewModel.Image;
                articleCreateDC.TwitterImage = articleCreateViewModel.TwitterImage;
                var id = this.User.FindFirst(ClaimTypes.NameIdentifier);
                string articleId=await _articleLogic.Save(id.Value,articleCreateDC);
                string url = this.Url.Action("Show", "Article", new { id = articleId });
                return Content(url);
            }
            catch (Exception ex)
            {
                await _logger.SendError(ex);
                List<string> articleNames = new List<string>();
                return Json(articleNames);
            }
        }

        public async Task Delete(string articleId)
        {
            try
            {
                var id = this.User.FindFirst(ClaimTypes.NameIdentifier);
                await _articleLogic.Delete(id.Value, articleId);
            }
            catch (Exception ex)
            {
                await _logger.SendError(ex);
            }
        }
    }
}