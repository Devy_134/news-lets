﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace news_lets.Migrations
{
    public partial class Subscribers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<List<string>>(
                name: "Subscribers",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Subscribers",
                table: "AspNetUsers");
        }
    }
}
