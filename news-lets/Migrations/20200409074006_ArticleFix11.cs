﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace news_lets.Migrations
{
    public partial class ArticleFix11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TwitterImage",
                table: "Articles",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "TwitterImage",
                table: "Articles");
        }
    }
}
