﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace news_lets.Migrations
{
    public partial class Articles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Articles",
                columns: table => new
                {
                    ArticleId = table.Column<string>(nullable: false),
                    AuthorId = table.Column<string>(nullable: true),
                    ArticleTitle = table.Column<string>(nullable: true),
                    ArticleText = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    LastEditDate = table.Column<DateTime>(nullable: false),
                    DiscoverImageUrl = table.Column<string>(nullable: true),
                    SingleWordDiscoverDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.ArticleId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Articles");
        }
    }
}
