﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace news_lets.Migrations
{
    public partial class DescriptionTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountLogo",
                table: "UserDescriptions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BackgroungSkin",
                table: "UserDescriptions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "UserDescriptions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserText",
                table: "UserDescriptions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountLogo",
                table: "UserDescriptions");

            migrationBuilder.DropColumn(
                name: "BackgroungSkin",
                table: "UserDescriptions");

            migrationBuilder.DropColumn(
                name: "UserName",
                table: "UserDescriptions");

            migrationBuilder.DropColumn(
                name: "UserText",
                table: "UserDescriptions");
        }
    }
}
