﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace news_lets.Migrations
{
    public partial class Topics3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string sql= @"INSERT INTO public.""Topics""(""TopicName"") VALUES('Culture');
            INSERT INTO public.""Topics""(""TopicName"") VALUES('Technology');
            INSERT INTO public.""Topics""(""TopicName"") VALUES('E-Drama');
            INSERT INTO public.""Topics""(""TopicName"") VALUES('Top-List');
            INSERT INTO public.""Topics""(""TopicName"") VALUES('Science');
            INSERT INTO public.""Topics""(""TopicName"") VALUES('Daily Life');";
            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
