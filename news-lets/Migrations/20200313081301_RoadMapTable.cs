﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace news_lets.Migrations
{
    public partial class RoadMapTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Roadmap",
                columns: table => new
                {
                    RoadmapId = table.Column<string>(nullable: false),
                    Version = table.Column<string>(nullable: true),
                    Done = table.Column<bool>(nullable: false),
                    Expected = table.Column<DateTime>(nullable: false),
                    Changes = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roadmap", x => x.RoadmapId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Roadmap");
        }
    }
}
