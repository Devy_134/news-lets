﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace news_lets.Migrations
{
    public partial class ArticleFix10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserArticle_AspNetUsers_ApplicationUserId",
                table: "ApplicationUserArticle");

            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserArticle_Articles_ArticleId",
                table: "ApplicationUserArticle");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationUserArticle",
                table: "ApplicationUserArticle");

            migrationBuilder.RenameTable(
                name: "ApplicationUserArticle",
                newName: "ApplicationUserArticles");

            migrationBuilder.RenameIndex(
                name: "IX_ApplicationUserArticle_ArticleId",
                table: "ApplicationUserArticles",
                newName: "IX_ApplicationUserArticles_ArticleId");

            migrationBuilder.RenameIndex(
                name: "IX_ApplicationUserArticle_ApplicationUserId",
                table: "ApplicationUserArticles",
                newName: "IX_ApplicationUserArticles_ApplicationUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationUserArticles",
                table: "ApplicationUserArticles",
                column: "ApplicationUserArticleId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserArticles_AspNetUsers_ApplicationUserId",
                table: "ApplicationUserArticles",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserArticles_Articles_ArticleId",
                table: "ApplicationUserArticles",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "ArticleId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserArticles_AspNetUsers_ApplicationUserId",
                table: "ApplicationUserArticles");

            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserArticles_Articles_ArticleId",
                table: "ApplicationUserArticles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationUserArticles",
                table: "ApplicationUserArticles");

            migrationBuilder.RenameTable(
                name: "ApplicationUserArticles",
                newName: "ApplicationUserArticle");

            migrationBuilder.RenameIndex(
                name: "IX_ApplicationUserArticles_ArticleId",
                table: "ApplicationUserArticle",
                newName: "IX_ApplicationUserArticle_ArticleId");

            migrationBuilder.RenameIndex(
                name: "IX_ApplicationUserArticles_ApplicationUserId",
                table: "ApplicationUserArticle",
                newName: "IX_ApplicationUserArticle_ApplicationUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationUserArticle",
                table: "ApplicationUserArticle",
                column: "ApplicationUserArticleId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserArticle_AspNetUsers_ApplicationUserId",
                table: "ApplicationUserArticle",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserArticle_Articles_ArticleId",
                table: "ApplicationUserArticle",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "ArticleId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
