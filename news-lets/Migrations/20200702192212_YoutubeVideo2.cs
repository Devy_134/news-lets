﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace news_lets.Migrations
{
    public partial class YoutubeVideo2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_YoutubeVideo_Articles_ArticleId",
                table: "YoutubeVideo");

            migrationBuilder.DropPrimaryKey(
                name: "PK_YoutubeVideo",
                table: "YoutubeVideo");

            migrationBuilder.RenameTable(
                name: "YoutubeVideo",
                newName: "Videos");

            migrationBuilder.RenameIndex(
                name: "IX_YoutubeVideo_ArticleId",
                table: "Videos",
                newName: "IX_Videos_ArticleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Videos",
                table: "Videos",
                column: "YoutubeVideoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Videos_Articles_ArticleId",
                table: "Videos",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "ArticleId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Videos_Articles_ArticleId",
                table: "Videos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Videos",
                table: "Videos");

            migrationBuilder.RenameTable(
                name: "Videos",
                newName: "YoutubeVideo");

            migrationBuilder.RenameIndex(
                name: "IX_Videos_ArticleId",
                table: "YoutubeVideo",
                newName: "IX_YoutubeVideo_ArticleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_YoutubeVideo",
                table: "YoutubeVideo",
                column: "YoutubeVideoId");

            migrationBuilder.AddForeignKey(
                name: "FK_YoutubeVideo_Articles_ArticleId",
                table: "YoutubeVideo",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "ArticleId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
