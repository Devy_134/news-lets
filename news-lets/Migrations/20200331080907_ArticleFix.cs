﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace news_lets.Migrations
{
    public partial class ArticleFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArticleText",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "ArticleTitle",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "DiscoverImageUrl",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "LastEditDate",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "SingleWordDiscoverDescription",
                table: "Articles");

            migrationBuilder.AddColumn<string>(
                name: "Alt",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AltText",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Text",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TwitterDescription",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TwitterTitle",
                table: "Articles",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Alt",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "AltText",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "Text",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "TwitterDescription",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "TwitterTitle",
                table: "Articles");

            migrationBuilder.AddColumn<string>(
                name: "ArticleText",
                table: "Articles",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ArticleTitle",
                table: "Articles",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AuthorId",
                table: "Articles",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DiscoverImageUrl",
                table: "Articles",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastEditDate",
                table: "Articles",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "SingleWordDiscoverDescription",
                table: "Articles",
                type: "text",
                nullable: true);
        }
    }
}
