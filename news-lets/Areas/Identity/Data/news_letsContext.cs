﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using news_lets.Entities;

namespace news_lets.Models
{
    public class news_letsContext : IdentityDbContext<ApplicationUser>
    {
        public news_letsContext(DbContextOptions<news_letsContext> options)
            : base(options)
        {
        }
        public DbSet<Article> articles { get; set; }
        public DbSet<UserDescription> userDescriptions { get; set; }
        public DbSet<ApplicationUser> ApplicationUsersArticles { get; set; }
        public DbSet<Roadmap> Roadmaps { get; set; }
        public DbSet<ApplicationUserArticle> ApplicationUserArticles { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<ArticleTopic> ArticleTopics { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<YoutubeVideo> YoutubeVideos { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Article>()
                .ToTable("Articles")
                .Property(e=>e.ArticleId)
                .ValueGeneratedOnAdd();
            builder.Entity<ApplicationUserArticle>()
                .ToTable("ApplicationUserArticles")
                .Property(e => e.ApplicationUserArticleId)
                .ValueGeneratedOnAdd();
            builder.Entity<Topic>().ToTable("Topics")
                .Property(e=>e.TopicId)
                .ValueGeneratedOnAdd();
            builder.Entity<ArticleTopic>().ToTable("ArticleTopics")
                .Property(e => e.ArticleTopicId)
                .ValueGeneratedOnAdd();
            builder.Entity<UserDescription>().ToTable("UserDescriptions");
            builder.Entity<Roadmap>().ToTable("Roadmap");
            builder.Entity<Comment>().ToTable("Comments");
            builder.Entity<YoutubeVideo>().ToTable("Videos");
            builder.Entity<UserDescription>()
            .Property(e => e.UserDescriptionId)
            .ValueGeneratedOnAdd();
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
