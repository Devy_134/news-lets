﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using news_lets.Entities;
using news_lets.Models;

[assembly: HostingStartup(typeof(news_lets.Areas.Identity.IdentityHostingStartup))]
namespace news_lets.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<news_letsContext>(options =>
                    options.UseNpgsql(
                        context.Configuration.GetConnectionString("news_letsContextConnection")));
          
                services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<news_letsContext>();
            });
        }
    }
}