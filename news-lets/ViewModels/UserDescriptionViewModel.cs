﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class UserDescriptionViewModel
    {
        public string UserName { get; set; }
        public string UserDescription { get; set; }
        public IFormFile UserSkin { get; set; }
        public IFormFile UserLogo { get; set; }
    }
}
