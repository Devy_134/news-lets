﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class AuthorNameId
    {
        public string AuthorName { get; set; }
        public string AuthorId { get; set; }
    }
}
