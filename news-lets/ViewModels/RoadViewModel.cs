﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class RoadViewModel
    {
        public string RoadmapId { get; set; }
        public string Version { get; set; }
        public bool Done { get; set; }
        public DateTime Expected { get; set; }
        public string Changes { get; set; }
    }
}
