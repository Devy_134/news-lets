﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class TopTopics
    {
        public string TopicName { get; set; }
        public int Occurences { get; set; }
    }
}
