﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class YoutubeVideo
    {
        public string VideoTitle { get; set; }
        public string VideoCode { get; set; }
    }
}
