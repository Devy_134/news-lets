﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class AuthorsShowViewModel
    {
        public List<AuthorViewModel> authorViewModels { get; set; }
    }
}
