﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class SubscriptionViewModel
    {
        public string articleId { get; set; }

        public string email { get; set; }
    }
}
