﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class ArticleNameId
    {
        public string ArticleName { get; set; }
        public string ArticleId { get; set; }
    }
}
