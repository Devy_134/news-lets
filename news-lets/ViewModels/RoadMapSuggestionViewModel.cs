﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class RoadMapSuggestionViewModel
    {
        public string Email { get; set; }
        public string Text { get; set; }
    }
}
