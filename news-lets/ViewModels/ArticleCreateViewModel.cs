﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class ArticleCreateViewModel
    {
        public string Title { get; set; }
        public string Text { get; set; }

        public IFormFile Image { get; set; }

        public string Alt { get; set; }

        public string AltText { get; set; }

        public string TwitterTitle { get; set; }

        public string TwitterDescription { get; set; }

        public string Topics { get;  set; }
        public List<YoutubeVideo> YoutubeVideo { get; set; }

        public IFormFile TwitterImage { get; set; }
    }
}
