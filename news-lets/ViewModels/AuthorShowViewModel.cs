﻿using news_lets.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class AuthorShowViewModel
    {
        public UserDescriptionJsonDC UserDescription { get; set; }

        public List<ArticleShowViewModel> articleShowViewModels { get; set; }
    }
}
