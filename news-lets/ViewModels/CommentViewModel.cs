﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class CommentViewModel
    {
        public string pseudonim { get; set; }
        public string comment { get; set; }
        public string articleId { get; set; }
    }
}
