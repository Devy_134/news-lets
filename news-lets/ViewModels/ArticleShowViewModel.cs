﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.ViewModels
{
    public class ArticleShowViewModel
    {
        public string ArticleId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string AuthorName { get; set; }
        public string AuthorId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ImagePath { get; set; }
        public string TwitterImage { get; set; }
        //TODO: Implement topics
        public string Alt { get; set; }
        public string AltText { get; set; }
        public string TwitterTitle { get; set; }
        public string TwitterDescription { get; set; }
        public List<ArticleNameId> ArticleNameIds { get; set; }
        public int Views { get; set; }
        public List<string> Topics { get; set; }
        public List<YoutubeVideo> YoutubeVideo { get; set; }
        public List<CommentViewModel> Comments { get; set; }
    }
}
