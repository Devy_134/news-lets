﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DataContracts
{
    public class AuthorShowDC
    {
        public UserDescriptionJsonDC UserDescription { get; set; }

        public List<ArticleShowDC> articleShowViewModels { get; set; }
    }
}
