﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DataContracts
{
    public class YoutubeVideoDC
    {
        public string VideoTitle { get; set; }
        public string VideoCode { get; set; }
    }
}
