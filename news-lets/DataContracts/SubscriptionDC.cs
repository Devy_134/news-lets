﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DataContracts
{
    public class SubscriptionDC
    {
        public string articleId { get; set; }

        public string email { get; set; }
    }
}
