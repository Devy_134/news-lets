﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DataContracts
{
    public class TopicsDC
    {
        public int TopicId { get; set; }

        public string TopicName { get; set; }
    }
}
