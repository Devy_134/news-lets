﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DataContracts
{
    public class UserDescriptionDC
    {
        public string UserName { get; set; }
        public string UserDescription { get; set; }
        public IFormFile UserSkin { get; set; }
        public IFormFile UserLogo { get; set; }
    }
}
