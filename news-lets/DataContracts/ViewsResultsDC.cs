﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DataContracts
{
    public class ViewsResultsDC
    {
        public string Id { get; set; }
        public string Views { get; set; }
        public string ValueType { get; set; }
    }
}
