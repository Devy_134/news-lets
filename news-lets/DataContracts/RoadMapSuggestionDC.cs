﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DataContracts
{
    public class RoadMapSuggestionDC
    {
        public string Email { get; set; }
        public string Text { get; set; }
    }
}
