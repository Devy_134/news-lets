﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DataContracts
{
    public class AuthorDC
    {
        public string ApplicationUserId { get; set; }
        public string UserName { get; set; }
        public string LogoUrl { get; set; }
        public int Subscribers { get; set; }
        public int ArticleCount { get; set; }
        public int Views { get; set; }
    }
}
