﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DataContracts
{
    public class TopTopicsDC
    {
        public string TopicName { get; set; }
        public int Occurences { get; set; }
    }
}
