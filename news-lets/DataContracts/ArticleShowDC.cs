﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DataContracts
{
    public class ArticleShowDC
    {
        public string ArticleId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ImagePath { get; set; }
        public string TwitterImage { get; set; }
        //TODO: Implement topics
        public string Alt { get; set; }
        public string AltText { get; set; }
        public string TwitterTitle { get; set; }
        public string TwitterDescription { get; set; }
        public int Views { get; set; }
        public List<YoutubeVideoDC> YoutubeVideo { get; set; }
        public List<string> Topics { get; set; }
    }
}
