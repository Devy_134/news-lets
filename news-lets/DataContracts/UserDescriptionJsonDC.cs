﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.DataContracts
{
    public class UserDescriptionJsonDC
    {
        public string UserName { get; set; }
        public string UserText { get; set; }
        public string AccountLogo { get; set; }
        public string BackgroungSkin { get; set; }
    }
}
