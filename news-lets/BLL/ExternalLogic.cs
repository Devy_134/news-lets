﻿using Mapster;
using Microsoft.AspNetCore.Hosting;
using news_lets.DataContracts;
using news_lets.Models;
using news_lets.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.Web.Sitemap;

namespace news_lets.BLL
{
    public class ExternalLogic : IExternalLogic
    {
        private readonly IExternalService _externalService;
        private readonly IArticleLogic _articleLogic;
        private readonly IHostingEnvironment _environment;

        public ExternalLogic(IExternalService externalService,IArticleLogic articleLogic, IHostingEnvironment environment)
        {
            _externalService = externalService;
            _articleLogic = articleLogic;
            _environment = environment;
        }

        public async Task UpdateViews(List<ViewsResults> viewsResults)
        {
            await _externalService.UpdateViews(viewsResults.Adapt<List<ViewsResultsDC>>());
        }

        public async Task GenerateSiteMapAsync(List<string> links, string siteUrl)
        {
            var sitemap = new Sitemap();

            //sitemap.Add(new Url
            //{
            //    ChangeFrequency = ChangeFrequency.Daily,
            //    Location = "http://www.example.com",
            //    Priority = 0.5,
            //    TimeStamp = DateTime.Now
            //});

            foreach(string link in links)
            { 
                sitemap.Add(CreateUrl(link,1.0));
            }

            foreach(string id in (await _articleLogic.GetArticlesList()).Select(x=>x.ArticleId))
            {
                sitemap.Add(CreateUrl(siteUrl + "Article/Show/" + id,0.9));
            }


            //Save sitemap structure to file
            sitemap.Save(_environment.WebRootPath + "/sitemap.xml");

            //Split a large list into pieces and store in a directory
            sitemap.SaveToDirectory(_environment.WebRootPath + "/sitemaps");
        }
        private static Url CreateUrl(string url,double priority)
        {
            return new Url
            {
                ChangeFrequency = ChangeFrequency.Daily,
                Location = url,
                Priority = priority,
                TimeStamp = DateTime.Now
            };
        }
    }
}
