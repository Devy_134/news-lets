﻿using Mapster;
using news_lets.Services;
using news_lets.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.BLL
{
    public class AuthorLogic : IAuthorLogic
    {
        private readonly IAuthorService _authorService;
        public AuthorLogic(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        public async Task<List<AuthorNameId>> GetAllAuthorNames()
        {
            return (await _authorService.GetAllAuthorNames()).Adapt<List<AuthorNameId>>();
        }

        public async Task<List<AuthorViewModel>> GetAllAuthorDescriptions()
        {
            return (await _authorService.GetAllAuthorDescriptions()).Adapt<List<AuthorViewModel>>();
        }

        public async Task<AuthorShowViewModel> GetAuthorData(string id)
        {
            return (await _authorService.GetAuthorShow(id)).Adapt<AuthorShowViewModel>();
        }
    }
}
