﻿using news_lets.DataContracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace news_lets.BLL
{
    public interface IAccountLogic
    {
        Task<int> GetSubscriberCount(string authorId);
        Task<List<string>> GetAllSubscribers(string authorId);
        Task<string[]> GetAccountNameId(string articleId);
        Task SaveDescription(string UserId, UserDescriptionDC userDescriptionDC);
        Task<UserDescriptionJsonDC> GetUserDescription(string userId);
    }
}