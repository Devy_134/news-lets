﻿using news_lets.DataContracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace news_lets.BLL
{
    public interface IRoadMapLogic
    {
        Task<List<RoadmapDC>> GetRoadMap();
        Task RoadMapSuggestion(RoadMapSuggestionDC roadMapSuggestionDC);
    }
}