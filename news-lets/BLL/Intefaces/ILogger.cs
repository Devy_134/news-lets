﻿using System;
using System.Threading.Tasks;

namespace news_lets.BLL
{
    public interface ILogger
    {
        Task SendError(Exception ex);
    }
}