﻿using news_lets.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace news_lets.BLL
{
    public interface IExternalLogic
    {
        Task UpdateViews(List<ViewsResults> viewsResults);
        Task GenerateSiteMapAsync(List<string> links, string siteUrl);
    }
}