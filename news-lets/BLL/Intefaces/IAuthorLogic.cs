﻿using news_lets.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace news_lets.BLL
{
    public interface IAuthorLogic
    {
        Task<List<AuthorNameId>> GetAllAuthorNames();
        Task<List<AuthorViewModel>> GetAllAuthorDescriptions();
        Task<AuthorShowViewModel> GetAuthorData(string id);
    }
}