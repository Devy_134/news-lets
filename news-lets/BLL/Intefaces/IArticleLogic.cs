﻿using news_lets.DataContracts;
using news_lets.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace news_lets.BLL
{
    public interface IArticleLogic
    {
        Task<ArticleShowDC> GetLatestNewsLetsArticle();
        Task<List<ArticleShowDC>> GetArticlesList();
        Task<List<(string, string)>> GetAllArticleNames();
        Task<List<TopTopicsDC>> GetMostPopularTopics();
        Task<List<TopicsDC>> GetTopics();
        Task<List<ArticleShowDC>> GetNewestArticles(int amount);
        Task<List<CommentDC>> GetComments(string articleId);
        Task Comment(CommentDC commentDC);
        Task Subscribe(SubscriptionDC subscriptionDC);
        Task Delete(string userId, string articleId);
        Task<List<string>> GetAllArticles(string id);
        Task<string> Save(string userId, ArticleCreateDC articleCreateDC);
        Task<ArticleShowDC> GetArticleData(string articleId);
        Task<List<ArticleShowDC>> GetSuggestArticles(List<string> topicIds);
    }
}