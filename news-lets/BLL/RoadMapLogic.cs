﻿using news_lets.DataContracts;
using news_lets.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.BLL
{
    public class RoadMapLogic : IRoadMapLogic
    {
        private readonly IRoadMapService _roadMapService;
        private readonly ITelegramWebHook _telegramWebHook;
        public RoadMapLogic(IRoadMapService roadMapService, ITelegramWebHook telegramWebHook)
        {
            _roadMapService = roadMapService;
            _telegramWebHook = telegramWebHook;
        }

        public async Task<List<RoadmapDC>> GetRoadMap()
        {          
            return await _roadMapService.GetRoadMap();
        }

        public async Task RoadMapSuggestion(RoadMapSuggestionDC roadMapSuggestionDC)
        {
            await _telegramWebHook.SendSuggestion(roadMapSuggestionDC);
        }
    }
}
