﻿using Mapster;
using news_lets.DataContracts;
using news_lets.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.BLL
{
    public class ArticleLogic : IArticleLogic
    {
        private readonly IArticleService _articleService;

        public ArticleLogic(IArticleService articleService)
        {
            _articleService = articleService;
        }

        public async Task<ArticleShowDC> GetLatestNewsLetsArticle()
        {
            return await _articleService.GetLatestNewsLetsArticle();
        }

        public async Task<List<ArticleShowDC>> GetArticlesList()
        {
            return await _articleService.GetArticlesList();
        }

        public async Task<List<(string, string)>> GetAllArticleNames()
        {
            return await _articleService.GetAllArticleNames();
        }

        public async Task<List<ArticleShowDC>> GetNewestArticles(int amount)
        {
            return await _articleService.GetNewestArticles(amount);
        }
        public async Task<List<string>> GetAllArticles(string id)
        {
            return await _articleService.GetArticleNames(id); ;
        }

        public async Task<List<TopTopicsDC>> GetMostPopularTopics()
        {
            return await _articleService.GetMostPopularTopics();
        }

        public async Task<string> Save(string userId, ArticleCreateDC articleCreateDC)
        {
            return await _articleService.Save(userId,articleCreateDC);
        }

        public async Task<List<TopicsDC>> GetTopics()
        {
            return await _articleService.GetTopics();
        }

        public async Task<ArticleShowDC> GetArticleData(string articleId)
        {
            return await _articleService.GetArticleData(articleId);
        }

        public async Task Subscribe(SubscriptionDC subscriptionDC)
        {
            await _articleService.Subscribe(subscriptionDC);
        }

        public async Task Delete(string userId, string articleId)
        {
            await _articleService.Delete(userId, articleId);
        }

        public async Task<List<CommentDC>> GetComments(string articleId)
        {
            return (await _articleService.GetComments(articleId)).Adapt<List<CommentDC>>();
        }

        public async Task Comment(CommentDC commentDC)
        {
            await _articleService.Comment(commentDC);
        }

        public async Task<List<ArticleShowDC>> GetSuggestArticles(List<string> topicNames)
        {
            return await _articleService.GetSuggestArticles(topicNames);
        }
    }
}
