﻿using news_lets.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.BLL
{
    public class Logger : ILogger
    {
        private readonly ITelegramWebHook _telegramWebHook;
        public Logger(ITelegramWebHook telegramWebHook)
        {
            _telegramWebHook = telegramWebHook;
        }

        public async Task SendError(Exception ex)
        {
            string error = "There was an *ERROR*:\n\n";
            if(ex.Message!=null)
            {
                error += $"*Message*:\n```{ex.Message}```\n";
            }
            if(ex.InnerException!=null)
            {
                error += $"*InnerException*:\n```{ex.InnerException}```\n";
            }
            if (ex.StackTrace != null)
            {
                error += $"*StackTrace*:\n```{ex.StackTrace}```\n";
            }
            await _telegramWebHook.SendError(error);
        }
    }
}
