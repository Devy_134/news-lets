﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using news_lets.DataContracts;
using news_lets.Services;

namespace news_lets.BLL
{
    public class AccountLogic : IAccountLogic
    {
        private readonly IAccountService _accountService;
        public AccountLogic(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public async Task<string[]> GetAccountNameId(string articleId)
        {
            return await _accountService.GetAccountNameId(articleId);
        }

        public async Task SaveDescription(string UserId, UserDescriptionDC userDescriptionDC)
        {
            await _accountService.SaveDescription(UserId,userDescriptionDC);
        }
        public async Task<UserDescriptionJsonDC> GetUserDescription(string userId)
        {
            return await _accountService.GetUserDescription(userId);
        }

        public async Task<int> GetSubscriberCount(string authorId)
        {
            return await _accountService.GetSubscriberAmount(authorId);
        }

        public async Task<List<string>> GetAllSubscribers(string authorId)
        {
            return await _accountService.GetAllSubscribers(authorId);
        }
    }
}
