﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.Enums
{
    public enum ImageTypeEnum
    {
        Logo = 1,
        PageBackground=2,
        ArticleImage=3,
        TwitterImage=4
    }
}
