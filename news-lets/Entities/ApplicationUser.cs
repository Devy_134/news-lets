﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace news_lets.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public ICollection<ApplicationUserArticle>? ApplicationUserArticles { get; set; }
        public UserDescription? UserDescription { get; set; }

        public List<string>? Subscribers { get; set; }
    }
}
