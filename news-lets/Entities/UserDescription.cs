﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.Entities
{
    public class UserDescription
    {
        public string UserDescriptionId { get; set; }

        public string AccountLogo { get; set; }

        public string BackgroungSkin { get; set; }

        public string UserName { get; set; }
        public int? Views { get; set; }
        public string UserText { get; set; }
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
