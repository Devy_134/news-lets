﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.Entities
{
    public class YoutubeVideo
    {
        public int YoutubeVideoId { get; set; }
        public string VideoTitle { get; set; }
        public string VideoCode { get; set; }
    }
}
