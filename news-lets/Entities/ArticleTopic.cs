﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.Entities
{
    public class ArticleTopic
    {
        public int ArticleTopicId { get; set; }

        public int TopicId { get; set; }

        public Topic Topic { get; set; }

        public string ArticleId { get; set; }

        public Article Article { get; set; }
    }
}
