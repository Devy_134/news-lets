﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace news_lets.Entities
{
    public class ApplicationUserArticle
    {
        public string ApplicationUserArticleId { get; set; }
        public string ArticleId { get; set; }
        public Article Article { get; set; }
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
